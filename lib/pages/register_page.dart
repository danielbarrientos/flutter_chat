
import 'package:chat/helpers/show_alert.dart';
import 'package:chat/services/auth_service.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/blue_Button.dart';
import 'package:chat/widgets/custom_input.dart';
import 'package:chat/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class RegisterPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height *0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(height: 20,),
                Logo(title: 'Register'),
                _Form(),
                _Labels(),
                Text('Terms and Conditions', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w300),),
              ],
            ),
          ),
        ),
      )
   );
  }
}



class _Form extends StatefulWidget {
  _Form({Key key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailController    = TextEditingController();
  final passwordController = TextEditingController();
  final nameController     = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final authservice = Provider.of<AuthService>(context);
    final socketservice = Provider.of<SocketService>(context);

    return Container(
      margin: EdgeInsets.only(top: 40,),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          CustomInput(
            icon: Icons.verified_user,
            placeholder: 'name',
            keyboardType: TextInputType.text,
            textController: nameController,
          ),
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'email',
            keyboardType: TextInputType.emailAddress,
            textController: emailController,
          ),
        
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'password',
            keyboardType: TextInputType.visiblePassword,
            textController: passwordController,
            isPassword: true,
          ),
          BlueButton(
            text: 'Register',
            onPressed: authservice.registering ? null : () async {
                FocusScope.of(context).unfocus();
                final registerOk = await  authservice.register(nameController.text.trim(), emailController.text.trim(), passwordController.text.trim());
                
                if(registerOk == true){
                  socketservice.connect();
                  Navigator.pushReplacementNamed(context, 'users');
                }
                else{
                  showAlert(context, 'Register incorrect', registerOk);
                }
              },
          )
        ],
      ),
    );
  }
}

class _Labels extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text("I have an account", style: TextStyle(color: Colors.black54,fontSize: 15, fontWeight: FontWeight.w400),),
          SizedBox(height: 10,),
          GestureDetector(
            child: Text(
              'Go to login', 
              style: TextStyle(color: Colors.blue[500],fontSize: 17, fontWeight: FontWeight.bold)
            ),
            onTap: (){
              Navigator.pushReplacementNamed(context,'login');
            },
          ) 
        ],
      ),
    );
  }
}