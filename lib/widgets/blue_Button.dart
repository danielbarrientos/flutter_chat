
import 'package:flutter/material.dart';

class BlueButton extends StatelessWidget {

  final String text;
  final Function onPressed;

  const BlueButton({
    @required this.text, 
    @required this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 2,
      highlightElevation: 5,
      color: Colors.blue,
      textColor: Colors.white,
      shape: StadiumBorder(),
      padding: EdgeInsets.only(left: 80,right: 80),
      child: Container(
        height: 50,
        child: Center(
          child: Text(this.text, style: TextStyle(fontSize: 17),),
        ),
      ),
      onPressed: this.onPressed
    );
  }
}