import 'dart:io';
import 'dart:ui';

import 'package:chat/models/messages_response.dart';
import 'package:chat/services/auth_service.dart';
import 'package:chat/services/chat_service.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/chat_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class ChatPage extends StatefulWidget {

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin{

  final _textController = new TextEditingController();
  final _focusNode = new FocusNode();

  List<ChatMessage> _messages = [];

  ChatService chatService;
  SocketService socketService;
  AuthService authService;

  bool _writing = false;

  @override
  void initState() {
    this.chatService =  Provider.of<ChatService>(context, listen: false);
    this.socketService =  Provider.of<SocketService>(context, listen: false);
    this.authService =  Provider.of<AuthService>(context, listen: false);

    this.socketService.socket.on('private-message',_listenMessage);

    _loadHistory(this.chatService.userFor.uid);

    super.initState();
  }

  void _loadHistory(String uid) async {

    List<Message> chat = await this.chatService.getMessages(uid);

    final history = chat.map((m) => new ChatMessage(
      text: m.message,
      uid: m.of,
      animationController: AnimationController(vsync: this,duration: Duration(milliseconds: 0))..forward(),
    ));

    _messages.insertAll(0, history);
    setState(() {
      
    });
  }

  void _listenMessage(dynamic payload){

    ChatMessage newMessage = new ChatMessage(
      text: payload['message'],
      uid: payload['of'],
      animationController: AnimationController(vsync: this, duration: Duration(milliseconds: 300)),
    );

    setState(() {
      _messages.insert(0, newMessage);
    });
    newMessage.animationController.forward();
  } 

  @override
  Widget build(BuildContext context) {
 
    final userFor = this.chatService.userFor;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        centerTitle: true,
        title: Column(
          children: [
            CircleAvatar(
              child: Text(userFor.name.substring(0,2).toUpperCase() ,style: TextStyle(fontSize: 13),),
              backgroundColor: Colors.blue[100],
              maxRadius: 13,
            ),
            SizedBox(height: 3,),
            Text(userFor.name, style: TextStyle(color: Colors.black54, fontSize: 12,),)
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                itemCount: _messages.length,
                itemBuilder: (_,i) => _messages[i],
                reverse: true,
              )
            ), 
            Divider(height: 3,),
            Container(
              color: Colors.white,
              child: _inputChat(),
            )
          ],
        )
     ),
   );
  }

  Widget _inputChat(){
    
    return SafeArea(
      child: Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: [
            Flexible(
              child: TextField(
                controller: _textController,
                onSubmitted: _handleSubmit,
                onChanged: (text){
               
                  setState(() {
                    if(text.trim().length > 0 ){
                      _writing = true;
                    }
                    else{
                      _writing = false;
                    }
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: 'Send message'
                ),
                focusNode: _focusNode,
              )
            ),
            //send Button
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              child: ! Platform.isIOS 
                ? CupertinoButton(
                  child: Text('Send'), 
                  onPressed: _writing
                    ? () =>_handleSubmit(_textController.text.trim())
                    : null
                  )
                : Container(
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  child: IconTheme(
                    data: IconThemeData(color: Colors.blue[400]),
                    child: IconButton(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      icon: Icon(Icons.send), 
                      onPressed: _writing
                        ? () =>_handleSubmit(_textController.text.trim())
                        : null
                    ),
                  ),
                )
            )
          ],
        ),
      )
    );
  }

  _handleSubmit(String text){

    if(text.length == 0) return;

    _textController.clear();
    _focusNode.requestFocus();

    final newMessage = new ChatMessage(
      uid: authService.user.uid, 
      text: text,
      animationController: AnimationController(vsync: this,duration: Duration(milliseconds: 300)),
    );

    _messages.insert(0, newMessage);
    newMessage.animationController.forward();

    setState(() {
      _writing = false;
    });

    this.socketService.emit('private-message',{
      'of': authService.user.uid,
      'for': this.chatService.userFor.uid,
      'message': text
    });

  }

 @override
  void dispose() {
    for( ChatMessage message in _messages){
      message.animationController.dispose();
    }
    this.socketService.socket.off('private-message');
    super.dispose();
  }
}